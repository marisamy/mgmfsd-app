package org.example.mgm;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class mgmgittest {
    private String testid;
    private String testName;

}
